import {NgRedux} from "ng2-redux";
import {Injectable} from "@angular/core";
import {ActionTypes} from "../constants/actionTypes";



//it is hack, because typeScript interface doesn`t have https://developer.mozilla.org/en/docs/Web/API/Fetch_API , but browsers it implement
declare global {
    interface Window {
        fetch:(url: string, options?: {}) => Promise<any>
    }
}

@Injectable()
export class RepositoriesActions {
    constructor(private ngRedux: NgRedux<any>) {
    }


    public selectRepository(repositoryIndex: number) {
        return {
            type: ActionTypes.SELECT_REPOSITORY,
            repositoryIndex
        }
    }

    public receiveRepositories(repositories: Array<Object>) {
        return {
            type: ActionTypes.RECEIVE_REPOSITORIES,
            repositories,
            isFetching: false,
            receivedAt: Date.now()
        }
    }

    public fetchRepository() {
        const {select} = this.ngRedux.getState();

        window.fetch(`https://api.github.com/search/repositories?q=language:${select.list[select.index]}&sort=stars&order=desc`)
            .then(res => res.json())
            .then(data => {
                this.ngRedux.dispatch(this.receiveRepositories(data.items));
            });

        return {
            type: ActionTypes.REQUEST_REPOSITORIES,
            isFetching: true
        }
    }
}
