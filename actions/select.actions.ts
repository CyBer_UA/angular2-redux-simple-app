import {Injectable} from "@angular/core";
import {NgRedux} from "ng2-redux";
import {IAppState} from "../reducers/index";
import {RepositoriesActions} from "./repositories.actions";
import {ActionTypes} from "../constants/actionTypes";

@Injectable()
export class SelectActions {
    constructor(private ngRedux: NgRedux<IAppState>,
                private repositories: RepositoriesActions) {
    }
    public languageIndexChange(selectedIndex) {
        return {
            type: ActionTypes.LANGUAGE_CHANGE,
            selectedIndex
        }
    }

    public receiveOptions(items) {
        return {
            type: ActionTypes.RECEIVE_OPTIONS,
            items
        }
    }

    public languageChange(selectedIndex) {
        this.ngRedux.dispatch(this.languageIndexChange(selectedIndex));
        return this.repositories.fetchRepository();
    }

    public fetchOptions() {
        setTimeout(() => {
            this.ngRedux.dispatch(this.receiveOptions(["js", "java", "c++"]));
            this.ngRedux.dispatch(this.repositories.fetchRepository());
        }, 200);

        return {
            type: ActionTypes.REQUEST_OPTIONS
        }
    }
}
