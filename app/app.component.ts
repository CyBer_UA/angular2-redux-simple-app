import {Component} from "@angular/core";
import {NgRedux, DevToolsExtension} from "ng2-redux";
import {IAppState, rootReducer} from "../reducers/index";
import {SelectActions} from "../actions/select.actions";
const createLogger = require('redux-logger');

@Component({
    selector: 'root',
    template: `
<label-repository></label-repository>
<select-languages></select-languages>
<repositories-list></repositories-list>`
})
export class App {
    constructor(private ngRedux: NgRedux<IAppState>,
                private selectActions: SelectActions,
                private devTool: DevToolsExtension) {

        this.ngRedux.configureStore(
            rootReducer,
            {},
            [createLogger()],
            [devTool.isEnabled() ? devTool.enhancer() : f => f]);

        this.ngRedux.dispatch(selectActions.fetchOptions());
    }
}
