import {NgModule} from "@angular/core";
import { FormsModule } from '@angular/forms';
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {NgReduxModule, DevToolsExtension} from "ng2-redux";
import {RepositoriesActions} from "../actions/repositories.actions";
import {SelectActions} from "../actions/select.actions";
import {App} from "./app.component";
import {RepositoriesList} from "../components/repositories/index";
import {RepositoriesPresentationList} from "../components/repositories/List";
import {SelectPresentation} from "../components/select/Select";
import {SelectLanguages} from "../components/select/index";
import {LabelPresentation} from "../components/label/Label";
import {LabelRepository} from "../components/label/index";
// import { Counter } from '../components/counter.component';
// import { Search } from '../components/search.component';

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        NgReduxModule.forRoot(),
        FormsModule
    ],
    declarations: [
        App,
        RepositoriesList,
        RepositoriesPresentationList,
        SelectPresentation,
        SelectLanguages,
        LabelRepository,
        LabelPresentation
        // CounterInfo,
        // Counter,
        // Search,
    ],
    bootstrap: [App],
    providers: [
        RepositoriesActions,
        SelectActions,
        DevToolsExtension,
    ]
})
export class AppModule {
}
