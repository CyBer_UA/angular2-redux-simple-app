import {Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'label-presentation',
    templateUrl: "components/label/template.html"
})
export class LabelPresentation {
    @Input()
    item: any;

    constructor() {}

    getImageSrc (item) {
        return `${item.owner.avatar_url}.jpg`;
    }
}
