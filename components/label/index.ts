import {Component} from "@angular/core";
import {IAppState} from "../../reducers/index";
import {NgRedux} from "ng2-redux";

@Component({
    selector: 'label-repository',
    template: `<label-presentation *ngIf="list[selected]" [item]="list[selected]"></label-presentation>`
})
export class LabelRepository {
    list: any;
    selected: number;

    constructor(private ngRedux: NgRedux<IAppState>) {
        ngRedux.connect(this.mapStateToTarget, null)(this);
    }


    mapStateToTarget(state: IAppState) {
        return {
            list: state.repositories.list,
            selected: state.repositories.selectedIndex
        };
    }
}
