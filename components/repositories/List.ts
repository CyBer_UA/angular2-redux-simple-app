import {Component, Input, EventEmitter, Output} from "@angular/core";

@Component({
    selector: 'repositories-presentation-list',
    templateUrl: "components/repositories/template.html"
})
export class RepositoriesPresentationList {
    @Input()
    items: any [];

    selected: any;

    private selectedIndex: number;

    @Output('onItemSelect')
    itemSelectEmitter = new EventEmitter<number>();

    constructor() {
    }

    private selectElement(index: number) {
        this.selectedIndex = index;
        this.itemSelectEmitter.emit(index);
    }

}
