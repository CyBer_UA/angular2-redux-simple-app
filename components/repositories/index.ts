import {Component} from "@angular/core";
import {IAppState} from "../../reducers/index";
import {NgRedux} from "ng2-redux";
import {RepositoriesActions} from "../../actions/repositories.actions";

@Component({
    selector: 'repositories-list',
    template: `<div *ngIf="isFetching">Loading...</div>
<repositories-presentation-list *ngIf="!isFetching"  [items]="items" (onItemSelect)="selectElement($event)">`
})
export class RepositoriesList {
    items: any [];

    isFetching: boolean;
    constructor(private ngRedux: NgRedux<IAppState>,
    private repositoriesActions: RepositoriesActions) {
       ngRedux.connect(this.mapStateToTarget, null)(this);
    }

    private selectElement(index: number) {
        this.ngRedux.dispatch(this.repositoriesActions.selectRepository(index));
    }

    mapStateToTarget(state: IAppState) {
        return {
            items: state.repositories.list,
            isFetching: state.repositories.isFetching
        };
    }
}
