import {Component, Input, EventEmitter, Output} from "@angular/core";

@Component({
    selector: 'select-presentation',
    templateUrl: "components/select/template.html"
})
export class SelectPresentation {
    @Input()
    items: String [];

    @Input()
    selected: number;

    @Output('onChange')
    changeEmitter = new EventEmitter<number>();

    constructor() {
    }

    private onChange({target}) {
        this.changeEmitter.emit(this.items.indexOf(target.value));
    }

    get value() {
        return this.items[this.selected];
    }

}
