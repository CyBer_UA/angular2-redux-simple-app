import {Component} from "@angular/core";
import {IAppState} from "../../reducers/index";
import {NgRedux} from "ng2-redux";
import {SelectActions} from "../../actions/select.actions";

@Component({
    selector: 'select-languages',
    template: `<select-presentation  [items]="items" [selected]="index" (onChange)="onChange($event)">`
})
export class SelectLanguages {
    items: any [];
    index: number;

    constructor(private ngRedux: NgRedux<IAppState>,
                private actions: SelectActions) {
        ngRedux.connect(this.mapStateToTarget, null)(this);
    }

    private onChange(index: number) {
        this.ngRedux.dispatch(this.actions.languageChange(index));
    }

    mapStateToTarget(state: IAppState) {
        return {
            items: state.select.list,
            index: state.select.index
        };
    }
}
