import { combineReducers } from 'redux';
// const persistState = require('redux-localstorage');
import { ISelectState, selectReducer } from './select';
import { IRepositoriesSelect, repositoriesReducer } from './repositories';

export class IAppState {
  select?: ISelectState;
  repositories?: IRepositoriesSelect;
};

export const rootReducer = combineReducers<IAppState>({
  select: selectReducer,
  repositories: repositoriesReducer
});
