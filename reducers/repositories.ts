import {ActionTypes} from "../constants/actionTypes";

export interface IRepositoriesSelect {
    selectedIndex: number;
    list: any[],
    isFetching: boolean
}

const INITIAL_STATE: IRepositoriesSelect = {
    selectedIndex: -1,
    list: [],
    isFetching: false
};

export function repositoriesReducer(state = INITIAL_STATE, action): IRepositoriesSelect {
    let item, newState = state;

    switch (action.type) {
        case ActionTypes.REQUEST_REPOSITORIES:
            newState = Object.assign({}, state, {
                isFetching: action.isFetching
            });

            break;

        case ActionTypes.RECEIVE_REPOSITORIES:
            newState = Object.assign({}, state, {
                list: action.repositories,
                isFetching: action.isFetching
            });

            if (newState.selectedIndex >= 0) {
                item = newState.list[newState.selectedIndex];
                newState.list[newState.selectedIndex] = Object.assign({}, item, {
                    selected: true
                });
            }

            break;

        case ActionTypes.SELECT_REPOSITORY:

            if (action.repositoryIndex >= 0) {
                item = state.list[action.repositoryIndex];
                if (state.selectedIndex >= 0) {
                    state.list[state.selectedIndex] = Object.assign({}, state.list[state.selectedIndex], {
                        selected: false
                    })
                }

                state.list[action.repositoryIndex] = Object.assign({}, item, {
                    selected: true
                })
            }
            newState = Object.assign({}, state, {
                selectedIndex: action.repositoryIndex
            });

            break;

    }

    return newState;
}
