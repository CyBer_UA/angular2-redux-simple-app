import {ActionTypes} from "../constants/actionTypes";

export interface ISelectState {
    index: number;
    list: any[],
}

const INIT_STATE: ISelectState = {
    list: [],
    index: 0
};

export function selectReducer(state = INIT_STATE, action): ISelectState {

    let newState = state;

    switch (action.type) {
        case ActionTypes.RECEIVE_OPTIONS:

            newState = Object.assign({}, state, {
                list: action.items
            });
            break;
        case ActionTypes.LANGUAGE_CHANGE:
            newState = Object.assign({}, state, {
                index: action.selectedIndex
            });

            break;
    }

    return newState;
}
